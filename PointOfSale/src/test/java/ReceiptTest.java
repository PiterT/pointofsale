import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReceiptTest {

    private static final int PRODUCT_PRICE1 = 100;
    private static final int PRODUCT_PRICE2 = 100;
    private static final String PRODUCT_NAME1 = "name1";
    private static final String PRODUCT_NAME2 = "name2";

    @Test
    void testAddProduct() {
        Receipt receipt = new Receipt();
        assertTrue(receipt.getProducts().isEmpty());

        Product product1 = new Product(PRODUCT_NAME1, PRODUCT_PRICE1);
        receipt.add(product1);
        assertEquals(1, receipt.getProducts().size());
        assertEquals(product1, receipt.getProducts().get(0));
    }

    @Test
    void shouldReturnTotalPriceForAllProduct() {
        Product product1 = new Product(PRODUCT_NAME1, PRODUCT_PRICE1);
        Product product2 = new Product(PRODUCT_NAME2, PRODUCT_PRICE2);
        Receipt receipt = new Receipt();
        receipt.add(product1);
        receipt.add(product2);
        int expectedPrice = PRODUCT_PRICE1 + PRODUCT_PRICE2;

        assertEquals(expectedPrice, receipt.getTotalPrice());
    }

    @Test
    void shouldReturnZeroWhenProductNoExists() {
        Receipt receipt = new Receipt();

        int expectedPrice = 0;

        assertEquals(expectedPrice, receipt.getTotalPrice());
    }

}