import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DisplayProductTest {

    private static final double PRODUCT_PRICE1 = 100;
    private static final double PRODUCT_PRICE2 = 200;

    private static final String PRODUCT_NAME1 = "name1";
    private static final String PRODUCT_NAME2 = "name2";

    @Test
    void shouldReturnProperString() {
        DisplayProduct displayProduct = new DisplayProduct();
        String message = displayProduct.displayOneProduct(new Product(PRODUCT_NAME1, PRODUCT_PRICE1));
        String message2 = displayProduct.displayOneProduct(new Product(PRODUCT_NAME2, PRODUCT_PRICE2));
        String expectedMessage = "Product name: " + PRODUCT_NAME1 + ", Product price: " + PRODUCT_PRICE1;

        assertEquals(expectedMessage, message);
        assertNotEquals(expectedMessage, message2);
    }
}