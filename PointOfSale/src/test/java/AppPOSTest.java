import com.sun.org.apache.regexp.internal.RE;
import devices.BarCodeScanner;
import devices.LCDDisplay;
import devices.Printer;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static junit.framework.TestCase.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

class AppPOSTest {

    private static final double PRODUCT_PRICE1 = 100;
    private static final double PRODUCT_PRICE2 = 200;

    private static final String PRODUCT_NAME1 = "name1";
    private static final String PRODUCT_NAME2 = "name2";

    private static final String PRODUCT_BARCODE1 = "barCode1";
    private static final String PRODUCT_BARCODE2 = "barCode2";

    private static final String PRODUCT_NOT_FOUND = "Product not found";
    private static final String INVALID_BAR_CODE = "Invalid bar-code";
    private static final String EXIT = "Exit";

    @Mock
    private BarCodeScanner barCodeScanner = mock(BarCodeScanner.class);

    @Mock
    private ProductDataAccessObject productDataAccessObject = mock(ProductDataAccessObject.class);

    @Mock
    private LCDDisplay lcdDisplay = mock(LCDDisplay.class);

    @Mock
    private Printer printer = mock(Printer.class);

    private AppPOS appPOS;
    private Receipt receipt;
    Product scannedProduct1 = new Product(PRODUCT_NAME1, PRODUCT_PRICE1, PRODUCT_BARCODE1);
    Product scannedProduct2 = new Product(PRODUCT_NAME2, PRODUCT_PRICE2, PRODUCT_BARCODE2);

    @BeforeEach
    private void setup() {
        receipt = new Receipt();
        appPOS = new AppPOS(barCodeScanner, productDataAccessObject, lcdDisplay, printer, receipt);
    }

    @Test
    void lcdDisplayShouldDisplayProductNotFoundWhenProductNotFound() {
        Receipt receipt = new Receipt();

        when(productDataAccessObject.getProduct(PRODUCT_BARCODE1)).thenReturn(scannedProduct1);
        when(barCodeScanner.readCode()).thenReturn(PRODUCT_BARCODE2);

        AppPOS appPOS1 = new AppPOS(barCodeScanner, productDataAccessObject, lcdDisplay, printer, receipt);

        appPOS1.startApp();

        verify(lcdDisplay).displayMessage(PRODUCT_NOT_FOUND);
    }

    @Test
    void lcdDisplayShouldDisplayProductIfProductExists() {
        when(barCodeScanner.readCode()).thenReturn(PRODUCT_BARCODE2);
        when(productDataAccessObject.getProduct(PRODUCT_BARCODE2)).thenReturn(scannedProduct1);

        appPOS.startApp();
        String message = new DisplayProduct().displayOneProduct(scannedProduct1);

        verify(lcdDisplay).displayMessage(message);
    }

    @Test
    void shouldAddOneProductToReceiptIfProductExists() {
        DisplayProduct displayProduct = new DisplayProduct();

        when(barCodeScanner.readCode()).thenReturn(PRODUCT_BARCODE1);
        when(productDataAccessObject.getProduct(PRODUCT_BARCODE1)).thenReturn(scannedProduct1);
        appPOS.startApp();

        String productExpected = displayProduct.displayOneProduct(receipt.getProducts().get(0));
        assertEquals(scannedProduct1.toString(), productExpected);
        assertTrue(receipt.getProducts().size() == 1);
    }

    @Test
    void shouldReturnInvalidBarCodeIfStringBarCodeIsEmpty() {
        AppPOS app = new AppPOS(barCodeScanner, productDataAccessObject, lcdDisplay, printer);

        when(barCodeScanner.readCode()).thenReturn("");

        app.startApp();

        verify(lcdDisplay).displayMessage(INVALID_BAR_CODE);
    }

    @Test
    void shouldReturnProductIfIsValidBarCode() {
        when(barCodeScanner.readCode()).thenReturn(PRODUCT_BARCODE1);
        when(productDataAccessObject.getProduct(PRODUCT_BARCODE1)).thenReturn(scannedProduct1);

        appPOS.startApp();

        String message = new DisplayProduct().displayOneProduct(scannedProduct1);

        verify(lcdDisplay).displayMessage(message);
        assertTrue(receipt.getProducts().get(0).equals(scannedProduct1));
        assertFalse(receipt.getProducts().get(0).equals(scannedProduct2));
    }

    @Test
    void shouldPrintALLProductsIfBarCodeIsEqualExit() {
        when(barCodeScanner.readCode()).thenReturn(PRODUCT_BARCODE1);
        when(productDataAccessObject.getProduct(PRODUCT_BARCODE1)).thenReturn(scannedProduct1);
        appPOS.startApp();
        verify(lcdDisplay).displayMessage("Product name: " + PRODUCT_NAME1 + ", " +
                "Product price: " + PRODUCT_PRICE1);

        when(barCodeScanner.readCode()).thenReturn(PRODUCT_BARCODE2);
        when(productDataAccessObject.getProduct(PRODUCT_BARCODE2)).thenReturn(scannedProduct2);
        appPOS.startApp();
        verify(lcdDisplay).displayMessage("Product name: " + PRODUCT_NAME2 + ", " +
                "Product price: " + PRODUCT_PRICE2);

        when(barCodeScanner.readCode()).thenReturn(EXIT);
        appPOS.startApp();

        assertTrue(receipt.getProducts().get(0).equals(scannedProduct1));
        assertTrue(receipt.getProducts().get(1).equals(scannedProduct2));

        String expectedMessge = "Product name: " + PRODUCT_NAME1 + ", Product price: " + PRODUCT_PRICE1
                + "\n " + "Product name: " + PRODUCT_NAME2 + ", Product price: " + PRODUCT_PRICE2;
        verify(printer).print(expectedMessge);
        verify(printer).print(String.valueOf(PRODUCT_PRICE1 + PRODUCT_PRICE2));
        verify(lcdDisplay).displayMessage(String.valueOf(PRODUCT_PRICE1 + PRODUCT_PRICE2));
    }

    @Test
    void shouldReturnReceiptIsEmptyWhenAnyProductHasNotScanned() {
        when(barCodeScanner.readCode()).thenReturn(EXIT);
        appPOS.startApp();

        String expectedMessage = "Receipt is empty";
        verify(lcdDisplay).displayMessage(expectedMessage);
    }
}