import java.util.List;
import java.util.stream.Collectors;

public class DisplayProduct {

    public String displayOneProduct(Product products)  {
      return  "Product name: " + products.getName() + ", Product price: " + products.getPrice();
    }

}
