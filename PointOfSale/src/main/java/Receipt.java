
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class Receipt {

    private List<Product> products = new ArrayList<>();

    public double getTotalPrice() {
        if (products != null) {
            return products.stream()
                    .mapToDouble(onePrice -> onePrice.getPrice())
                    .sum();
        } else {
            return 0;
        }
    }

    public void add(Product product) {
        products.add(product);
    }


    public String showReceipt() {
        return products.stream().map(Object::toString)
                .collect(Collectors.joining("\n "));
    }


}
