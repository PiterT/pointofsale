public enum MessageLCD {
    PRODUCT_NOT_FOUND("Product not found"),
    INVALID_BAR_CODE("Invalid bar-code"),
    EXIT("Exit");

    private String decription;

    MessageLCD(String decription) {
        this.decription = decription;
    }

    public String getDecription() {
        return decription;
    }

}
