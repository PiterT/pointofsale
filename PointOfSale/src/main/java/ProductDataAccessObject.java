
public interface ProductDataAccessObject {

    Product getProduct(String barCode);

}
