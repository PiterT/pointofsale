import devices.BarCodeScanner;
import devices.LCDDisplay;
import devices.Printer;
import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
public class AppPOS {

    private BarCodeScanner barCodeScanner;
    private ProductDataAccessObject productDataAccessObject;
    private LCDDisplay lcdDisplay;
    private Printer printer;
    private Receipt receipt = new Receipt();

    protected AppPOS(BarCodeScanner barCodeScanner, ProductDataAccessObject productDataAccessObject, LCDDisplay lcdDisplay, Printer printer) {
        this.barCodeScanner = barCodeScanner;
        this.productDataAccessObject = productDataAccessObject;
        this.lcdDisplay = lcdDisplay;
        this.printer = printer;
    }

    public void startApp() {
        String stringBarCode = scanBarCode();

        if (!scanBarCode().equals(MessageLCD.EXIT)) {
            isValidBarCode(stringBarCode);
        }
        isExit(stringBarCode);
    }
    private String scanBarCode() {
        return barCodeScanner.readCode();
    }

    private void isValidBarCode(String stringBarCode) {
        if (stringBarCode.isEmpty()) {
            lcdDisplay.displayMessage(MessageLCD.INVALID_BAR_CODE.getDecription());
        } else {
            scanProduct(stringBarCode);
        }
    }

    private void scanProduct(String barCode) {
        Optional<Product> optionalProduct = findProductByBarCode(barCode);

        if (optionalProduct.isPresent()) {
            productFound(optionalProduct.get());
        } else {
            productNotFound();
        }
    }

    private Optional<Product> findProductByBarCode(String barCode) {
        return Optional.ofNullable(productDataAccessObject.getProduct(barCode));
    }

    private void productFound(Product product) {
        receipt.add(product);
        lcdDisplay.displayMessage(new DisplayProduct().displayOneProduct(product));
    }

    private void productNotFound() {
        lcdDisplay.displayMessage(MessageLCD.PRODUCT_NOT_FOUND.getDecription());
    }

    private void endMakeReceipt() {
        if (receipt.getProducts().size() > 0) {
            printer.print(receipt.showReceipt());
            printer.print(String.valueOf(receipt.getTotalPrice()));
            lcdDisplay.displayMessage(String.valueOf(receipt.getTotalPrice()));
        } else {
            lcdDisplay.displayMessage(("Receipt is empty"));
        }
    }

    private void isExit(String barCode) {
        if (MessageLCD.EXIT.getDecription().equals(barCode)) {
            endMakeReceipt();
        }
    }


} //end class
