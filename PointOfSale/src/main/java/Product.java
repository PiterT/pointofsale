import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Product {

    String name;
    double price;
    String barCode;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public Product(String name, double price, String barCode) {
        this.name = name;
        this.price = price;
        this.barCode = barCode;
    }

    public String toString() {
        return "Product name: " + getName() + ", Product price: " + getPrice();
    }

}
