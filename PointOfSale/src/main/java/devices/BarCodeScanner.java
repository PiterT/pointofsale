package devices;

public interface BarCodeScanner {

    String readCode() ;
}
